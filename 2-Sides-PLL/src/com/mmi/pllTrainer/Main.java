
package com.mmi.pllTrainer;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import com.mmi.pllTrainer.gui.controller.MainController;

/**
 * @author Michael Mikolajczyk
 * @version $Revision: $, $Date: $, $Author: $
 */
public class Main {

  public static void main(String[] args) {
    SwingUtilities.invokeLater(new Runnable() {

      @Override
      public void run() {
        // System look and feel
        try {
          UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
          System.err.println(e.getStackTrace());
        }

        MainController mainController = new MainController();
        mainController.startView();
      }

    });
  }
}
