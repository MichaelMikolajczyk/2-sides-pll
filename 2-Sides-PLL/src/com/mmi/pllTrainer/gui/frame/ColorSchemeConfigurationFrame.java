
package com.mmi.pllTrainer.gui.frame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import com.mmi.pllTrainer.gui.model.CubeModel;
import com.mmi.pllTrainer.gui.util.Utilities;
import com.mmi.pllTrainer.puzzle.RubiksCube;
import com.mmi.pllTrainer.puzzle.common.Algo;
import com.mmi.pllTrainer.puzzle.common.ColorScheme;
import com.puzzletimer.graphics.Panel3D;

/**
 * @author Michael Mikolajczyk
 * @version $Revision: $, $Date: $, $Author: $
 */
public class ColorSchemeConfigurationFrame extends JDialog {
  private static final long serialVersionUID = 1L;

  private CubeModel cubeModel;
  private Panel3D pnl3d;
  private ColorScheme tmpColorScheme;
  private final Dimension lblColorDim = new Dimension(100, 30);
  private final Dimension btnDim = new Dimension(100, 30);

  private Map<String, JLabel> colorLabels = new HashMap<String, JLabel>();

  public ColorSchemeConfigurationFrame(JFrame mainFrame, CubeModel cubeModel) {
    super(mainFrame, "Color scheme...", true);
    this.cubeModel = cubeModel;
    this.pnl3d = new Panel3D();
    this.tmpColorScheme = this.cubeModel.getColorScheme().clone();

    setLayout(new BorderLayout());
    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

    // Restore button
    URL resource = getClass().getResource("/com/mmi/pllTrainer/gui/resources/icon/go-back-icon.png");
    ImageIcon icnRestore = new ImageIcon(resource);
    JButton btnDefault = new JButton("Restore default values");
    btnDefault.setIcon(icnRestore);
    btnDefault.setFont(Utilities.createFontLabel());
    btnDefault.setIconTextGap(20);
    btnDefault.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        ColorScheme defaultColorScheme = new ColorScheme();
        ColorScheme colorScheme = ColorSchemeConfigurationFrame.this.tmpColorScheme;
        for (String face : ColorScheme.getSortedFaces()) {
          colorScheme.updateColorForFace(face, defaultColorScheme.getColorForFace(face));
        }
        ColorSchemeConfigurationFrame.this.updateView();
      }
    });
    add(btnDefault, BorderLayout.PAGE_START);

    // Cube View
    pnl3d.setMesh(RubiksCube.getScrambledPuzzleMesh(this.tmpColorScheme, new Algo()));
    pnl3d.setPreferredSize(new Dimension(600, 220));
    add(pnl3d, BorderLayout.CENTER);

    // Edit colors
    JPanel pnlMain = new JPanel(new BorderLayout());
    createEditFaceLabels(pnlMain); // Center
    createButtons(pnlMain); // Page End

    add(pnlMain, BorderLayout.PAGE_END);
  }

  private void createButtons(JPanel pnlMain) {
    JPanel pnlButton = new JPanel();
    pnlButton.setLayout(new FlowLayout(FlowLayout.RIGHT, 10, 5));

    JButton btnApply = new JButton("Apply");
    btnApply.setPreferredSize(btnDim);
    btnApply.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        ColorScheme colorScheme = ColorSchemeConfigurationFrame.this.tmpColorScheme;
        for (String face : ColorScheme.getSortedFaces()) {
          Color newColor = colorScheme.getColorForFace(face);
          ColorSchemeConfigurationFrame.this.cubeModel.updateColorForFace(face, newColor);
        }
        ColorSchemeConfigurationFrame.this.dispose();
      }
    });
    pnlButton.add(btnApply);

    JButton btnCancel = new JButton("Cancel");
    btnCancel.setPreferredSize(btnDim);
    btnCancel.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        ColorSchemeConfigurationFrame.this.dispose();
      }
    });
    pnlButton.add(btnCancel);

    pnlMain.add(pnlButton, BorderLayout.PAGE_END);
  }

  private void createEditFaceLabels(JPanel pnlMain) {
    JPanel pnlColorEdit = new JPanel();
    pnlColorEdit.setLayout(new GridLayout(3, 2, 10, 10));
    pnlColorEdit.setBorder(Utilities.createTitledBorder("Edit face colors"));

    for (final String face : ColorScheme.getSortedFaces()) {
      final Color faceColor = this.tmpColorScheme.getColorForFace(face);
      JLabel lblColor = new JLabel();
      lblColor.setFont(Utilities.createFontLabel());
      lblColor.setHorizontalAlignment(SwingConstants.CENTER);
      lblColor.setToolTipText(face + ": Click to edit...");
      lblColor.setOpaque(true);
      lblColor.setPreferredSize(lblColorDim);
      lblColor.setBackground(faceColor);
      lblColor.setBorder(BorderFactory.createMatteBorder(4, 4, 4, 15, getBackground()));
      //      lblColor.addMouseListener(new ChangeFaceColorListener(ColorSchemeConfigurationFrame.this, this.tmpColorScheme, face));
      lblColor.addMouseListener(new MouseAdapter() {
        @Override
        public void mouseReleased(MouseEvent e) {
          super.mouseReleased(e);
          Color newColor = JColorChooser.showDialog(ColorSchemeConfigurationFrame.this, "Edit " + face, faceColor);
          if (newColor != null) {
            ColorSchemeConfigurationFrame.this.tmpColorScheme.updateColorForFace(face, newColor);
            ColorSchemeConfigurationFrame.this.updateView();
          }
        }
      });
      pnlColorEdit.add(lblColor);
      colorLabels.put(face, lblColor);
    }

    pnlMain.add(pnlColorEdit, BorderLayout.CENTER);
  }

  public void showView() {
    pack();
    setLocation(Utilities.getCenterPosition(this));
    setVisible(true);
  }

  public void updateView() {
    this.pnl3d.setMesh(RubiksCube.getScrambledPuzzleMesh(this.tmpColorScheme, new Algo()));

    for (String face : ColorScheme.getSortedFaces()) {
      JLabel lbl = colorLabels.get(face);
      lbl.setBackground(this.tmpColorScheme.getColorForFace(face));
    }
  }

}
