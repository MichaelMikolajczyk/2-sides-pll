
package com.mmi.pllTrainer.gui.frame;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JPanel;

import com.mmi.pllTrainer.gui.model.CubeModel;
import com.mmi.pllTrainer.gui.view.ConfigurationView;
import com.mmi.pllTrainer.gui.view.CubeView;
import com.mmi.pllTrainer.gui.view.MenuBar;
import com.mmi.pllTrainer.gui.view.PLLView;

/**
 * @author <a href="mailto:mikolajczyk@ssi-schaefer-noell.com">mikolajczyk</a>
 * @version $Revision: $, $Date: $, $Author: $
 */
public class MainFrame extends JFrame {
  private static final long serialVersionUID = 1L;

  private CubeModel cubeModel;

  public MainFrame(String title, CubeView cubeView, CubeModel cubeModel) {
    super(title);
    this.cubeModel = cubeModel;
    setMinimumSize(new Dimension(900, 700));

    setLayout(new GridLayout(1, 2));

    JMenuBar menuBar = new MenuBar(this, this.cubeModel);
    setJMenuBar(menuBar);

    // LeftPanel
    JPanel leftPanel = createLeftPanel(cubeView);

    // RightPanel
    JPanel rightPanel = createRightPanel();

    // Add to Main-Frame
    add(leftPanel);
    add(rightPanel);
  }

  private JPanel createLeftPanel(CubeView cubeView) {
    JPanel panel = new JPanel(new BorderLayout(0, 5));

    // Configuration
    JPanel topPanel = new ConfigurationView(cubeModel);
    panel.add(topPanel, BorderLayout.PAGE_START);

    panel.add(cubeView, BorderLayout.CENTER);
    return panel;
  }

  private JPanel createRightPanel() {
    JPanel panel = new JPanel(new BorderLayout(0, 5));

    PLLView pllView = new PLLView(this.cubeModel);
    panel.add(pllView, BorderLayout.CENTER);

    // TODO mmi: Add Info ViewPart if Category is implemented
    // InfoView infoView = new InfoView(this.cubeModel);
    // panel.add(infoView, BorderLayout.PAGE_END);
    return panel;
  }
}
