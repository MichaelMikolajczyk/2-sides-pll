/*
 * Copyright (c) 2014 SSI Schaefer Noell GmbH
 *
 * $Id: $ / $HeadURL: $
 */

package com.mmi.pllTrainer.gui.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import com.mmi.pllTrainer.gui.model.Category;
import com.mmi.pllTrainer.puzzle.common.Algo;
import com.mmi.pllTrainer.puzzle.common.PLL;

/**
 * @author <a href="mailto:mikolajczyk@ssi-schaefer-noell.com">mikolajczyk</a>
 * @version $Revision: $, $Date: $, $Author: $
 */
public class PLLManager {

  private Category category;
  private PLL pll;
  private static Random RANDOM = new Random();

  public PLLManager() {
    this.pll = PLL.A1;
    this.category = Category.CATEGORY_ALL;
  }

  public PLL getNextPLL() {
    switch (this.category) {
      case CATEGORY_ALL:
        randomizePLL(PLL.values());
        break;
    }
    return pll;
  }

  private void randomizePLL(PLL[] plls) {
    PLL oldPLL = this.pll;
    while (this.pll == oldPLL) {
      int randomIndex = RANDOM.nextInt(plls.length);
      this.pll = plls[randomIndex];
    }
  }

  public Algo getAlgo() {
    return this.pll.getAlgo();
  }

  public PLL getPLL() {
    return this.pll;
  }

  public List<PLL> getPLLsForCategory() {
    List<PLL> retVal = Arrays.asList(PLL.values());

    switch (this.category) {
      case CATEGORY_ALL:
        break;
    }
    return retVal;
  }

  public Category getCategory() {
    return category;
  }

  public void setCategory(Category category) {
    this.category = category;
  }

}
