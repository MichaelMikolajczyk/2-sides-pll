
package com.mmi.pllTrainer.gui.controller;

import com.mmi.pllTrainer.gui.frame.MainFrame;
import com.mmi.pllTrainer.gui.listener.ResetCubeViewListener;
import com.mmi.pllTrainer.gui.listener.SpaceListener;
import com.mmi.pllTrainer.gui.listener.WindowCloseListener;
import com.mmi.pllTrainer.gui.model.CubeModel;
import com.mmi.pllTrainer.gui.util.Utilities;
import com.mmi.pllTrainer.gui.view.CubeView;

/**
 * @author Michael Mikolajczyk
 * @version $Revision: $, $Date: $, $Author: $
 */
public class MainController {

  // Main-Frame
  private MainFrame frame;
  // Views
  private CubeView cubeView;
  // Models
  private CubeModel cubeModel;

  public MainController() {

    // models
    this.cubeModel = new CubeModel();

    // views
    this.cubeView = new CubeView();

    // Main-Frame
    this.frame = new MainFrame("2-Sides-PLL", cubeView, cubeModel);

    // Listeners
    this.frame.addKeyListener(new ResetCubeViewListener(this.cubeModel));
    this.frame.addKeyListener(new SpaceListener(this.cubeModel));
    this.frame.addWindowListener(new WindowCloseListener(this.cubeModel));

    // Observers
    this.cubeModel.addObserver(this.cubeView);
  }

  public void startView() {
    this.frame.pack();
    this.frame.setLocation(Utilities.getCenterPosition(this.frame));
    this.frame.setVisible(true);
  }

}
