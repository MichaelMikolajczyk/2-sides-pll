
package com.mmi.pllTrainer.gui.util;

import java.awt.Component;
import java.awt.Font;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.BorderFactory;
import javax.swing.border.TitledBorder;

/**
 * @author Michael Mikolajczyk
 * @version $Revision: $, $Date: $, $Author: $
 */
public class Utilities {

  public static TitledBorder createTitledBorder(String title) {
    TitledBorder border = BorderFactory.createTitledBorder(title);
    border.setTitleFont(createFontBorder());
    return border;
  }

  private static Font createFontBorder() {
    return createFont(Font.ITALIC, 12);
  }

  public static Font createFont(int style, int size) {
    return new Font("Helvetica", style, size);
  }

  public static Font createFontLabel() {
    return createFont(Font.PLAIN, 12);
  }

  public static Point getCenterPosition(Component component) {
    int x = (int) ((Toolkit.getDefaultToolkit().getScreenSize().getWidth() / 2) - component.getSize().getWidth() / 2);
    int y = (int) ((Toolkit.getDefaultToolkit().getScreenSize().getHeight() / 2) - component.getSize().getHeight() / 2);
    return new Point(x, y);
  }

}
