
package com.mmi.pllTrainer.gui.listener;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JCheckBox;

import com.mmi.pllTrainer.gui.model.CubeModel;
import com.mmi.pllTrainer.puzzle.common.LastLayerMove;

/**
 * @author Michael Mikolajczyk
 * @version $Revision: $, $Date: $, $Author: $
 */
public class LastLayerMoveConfigListener implements ItemListener {

  private CubeModel cubeModel;

  public LastLayerMoveConfigListener(CubeModel cubeModel) {
    this.cubeModel = cubeModel;
  }

  @Override
  public void itemStateChanged(ItemEvent e) {
    JCheckBox box = (JCheckBox) e.getItem();
    String selectionText = box.getText();
    LastLayerMove lastLayerMove = LastLayerMove.NO;
    for (LastLayerMove move : LastLayerMove.values()) {
      if (move.getSelectionText().equals(selectionText)) {
        lastLayerMove = move;
        break;
      }
    }
    cubeModel.setLastLayerMove(lastLayerMove);
  }

}
