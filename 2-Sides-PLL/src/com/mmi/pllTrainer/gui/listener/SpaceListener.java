
package com.mmi.pllTrainer.gui.listener;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import com.mmi.pllTrainer.gui.model.CubeModel;

/**
 * @author Michael Mikolajczyk
 * @version $Revision: $, $Date: $, $Author: $
 */
public class SpaceListener extends KeyAdapter {

  private CubeModel cubeModel;

  public SpaceListener(CubeModel cubeModel) {
    this.cubeModel = cubeModel;
  }

  @Override
  public void keyReleased(KeyEvent e) {
    if (e.getKeyCode() == KeyEvent.VK_SPACE) {
      this.cubeModel.nextPLL();
    }
  }

}
