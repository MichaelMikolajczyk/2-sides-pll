
package com.mmi.pllTrainer.gui.listener;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import com.mmi.pllTrainer.gui.model.CubeModel;

/**
 * @author Michael Mikolajczyk
 * @version $Revision: $, $Date: $, $Author: $
 */
public class ResetCubeViewListener extends KeyAdapter {

  private CubeModel cubeModel;

  public ResetCubeViewListener(CubeModel cubeModel) {
    this.cubeModel = cubeModel;
  }

  @Override
  public void keyReleased(KeyEvent e) {
    if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
      cubeModel.resetCubeView();
    }
  }

}
