
package com.mmi.pllTrainer.gui.listener;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JCheckBox;

import com.mmi.pllTrainer.gui.model.CubeModel;
import com.mmi.pllTrainer.puzzle.common.CubeRotation;

/**
 * @author Michael Mikolajczyk
 * @version $Revision: $, $Date: $, $Author: $
 */
public class CubeRotationConfigListener implements ItemListener {

  private CubeModel cubeModel;

  public CubeRotationConfigListener(CubeModel cubeModel) {
    this.cubeModel = cubeModel;
  }

  @Override
  public void itemStateChanged(ItemEvent e) {
    JCheckBox box = (JCheckBox) e.getItem();
    String selectionText = box.getText();
    CubeRotation cubeRotation = CubeRotation.NO;
    for (CubeRotation rotation : CubeRotation.values()) {
      if (rotation.getSelectionText().equals(selectionText)) {
        cubeRotation = rotation;
        break;
      }
    }
    cubeModel.setCubeRotation(cubeRotation);
  }

}
