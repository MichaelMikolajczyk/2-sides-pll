
package com.mmi.pllTrainer.gui.listener;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import com.mmi.pllTrainer.gui.model.CubeModel;

/**
 * @author Michael Mikolajczyk
 * @version $Revision: $, $Date: $, $Author: $
 */
public class WindowCloseListener extends WindowAdapter {

  private CubeModel cubeModel;

  public WindowCloseListener(CubeModel cubeModel) {
    this.cubeModel = cubeModel;
  }

  @Override
  public void windowClosing(WindowEvent e) {
    super.windowClosing(e);
    System.out.println("Closing Application");
    System.exit(0);
    // TODO mmi: Save config
  }

  @Override
  public void windowOpened(WindowEvent e) {
    System.out.println("Starting Application");
    super.windowOpened(e);
    this.cubeModel.nextPLL();
  }

}
