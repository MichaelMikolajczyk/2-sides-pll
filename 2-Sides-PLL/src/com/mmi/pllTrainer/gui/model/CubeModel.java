
package com.mmi.pllTrainer.gui.model;

import java.awt.Color;
import java.util.List;
import java.util.Observable;

import com.mmi.pllTrainer.gui.controller.PLLManager;
import com.mmi.pllTrainer.puzzle.common.Algo;
import com.mmi.pllTrainer.puzzle.common.ColorScheme;
import com.mmi.pllTrainer.puzzle.common.CubeRotation;
import com.mmi.pllTrainer.puzzle.common.LastLayerMove;
import com.mmi.pllTrainer.puzzle.common.PLL;

/**
 * @author Michael Mikolajczyk
 * @version $Revision: $, $Date: $, $Author: $
 */
public class CubeModel extends Observable {

  private ColorScheme colorScheme;
  private CubeRotation cubeRotation;
  private LastLayerMove lastLayerMove;
  private PLLManager pllManager;

  public CubeModel() {
    this.colorScheme = new ColorScheme();
    this.cubeRotation = CubeRotation.NO;
    this.lastLayerMove = LastLayerMove.NO;

    this.pllManager = new PLLManager();
  }

  public void nextPLL() {
    this.pllManager.getNextPLL();

    setChanged();
    notifyObservers(this);
  }

  public void resetCubeView() {
    setChanged();
    notifyObservers(this);
  }

  public Algo getAlgo() {
    return this.pllManager.getAlgo();
  }

  public ColorScheme getColorScheme() {
    return this.colorScheme;
  }

  public CubeRotation getCubeRotation() {
    return cubeRotation;
  }

  public void setCubeRotation(CubeRotation cubeRotation) {
    this.cubeRotation = cubeRotation;

    setChanged();
    notifyObservers(this);
  }

  public LastLayerMove getLastLayerMove() {
    return lastLayerMove;
  }

  public void changeCategory(Category cat) {
    if (this.pllManager.getCategory() != cat) {
      System.out.println("Change Category <" + cat.name() + ">");
      this.pllManager.setCategory(cat);
      this.pllManager.getNextPLL();
    }
    setChanged();
    notifyObservers(this);
  }

  public void setLastLayerMove(LastLayerMove lastLayerMove) {
    this.lastLayerMove = lastLayerMove;

    setChanged();
    notifyObservers(this);
  }

  @Override
  public String toString() {
    return "[CubeRotation=" + this.cubeRotation + ", LastLayerMove=" + this.lastLayerMove + ", PLL=" + this.pllManager.getPLL() + "]";
  }

  public void updateColorForFace(String face, Color newColor) {
    getColorScheme().updateColorForFace(face, newColor);

    setChanged();
    notifyObservers(this);
  }

  public List<PLL> getDisplayAblePLLs() {
    return this.pllManager.getPLLsForCategory();
  }

  public PLL getPLL() {
    return pllManager.getPLL();
  }

  public Category getCategory() {
    return this.pllManager.getCategory();
  }

}
