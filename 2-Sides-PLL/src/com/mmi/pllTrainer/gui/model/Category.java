/*
 * Copyright (c) 2014 SSI Schaefer Noell GmbH
 *
 * $Id: $ / $HeadURL: $
 */

package com.mmi.pllTrainer.gui.model;

/**
 * @author <a href="mailto:mikolajczyk@ssi-schaefer-noell.com">mikolajczyk</a>
 * @version $Revision: $, $Date: $, $Author: $
 */
public enum Category {
  CATEGORY_ALL("All Categories"), //
    CATEGORY_A("Category A"), //
    CATEGORY_B("Category B"), //
    CATEGORY_C("Category C"), //
    CATEGORY_D("Category D");

  private String text;

  private Category(String text) {
    this.text = text;
  }

  public String getText() {
    return text;
  }
}
