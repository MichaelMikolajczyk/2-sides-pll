
package com.mmi.pllTrainer.gui.view;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JPanel;

import com.mmi.pllTrainer.gui.util.Utilities;

/**
 * @author Michael Mikolajczyk
 * @version $Revision: $, $Date: $, $Author: $
 */
public class PLLPanel extends JPanel {
  private static final long serialVersionUID = 1L;

  public PLLPanel() {
    add(new JLabel("PlatzHalter"));
    setBorder(Utilities.createTitledBorder("Permutations of Last Layer"));
    setBackground(Color.RED);
  }

}
