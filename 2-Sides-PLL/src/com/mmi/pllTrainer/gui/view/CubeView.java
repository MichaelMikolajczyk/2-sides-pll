
package com.mmi.pllTrainer.gui.view;

import java.awt.BorderLayout;
import java.util.Observable;
import java.util.Observer;

import com.mmi.pllTrainer.gui.model.CubeModel;
import com.mmi.pllTrainer.gui.util.Utilities;
import com.mmi.pllTrainer.puzzle.RubiksCube;
import com.mmi.pllTrainer.puzzle.common.Algo;
import com.mmi.pllTrainer.puzzle.common.ColorScheme;
import com.mmi.pllTrainer.puzzle.common.CubeRotation;
import com.mmi.pllTrainer.puzzle.common.LastLayerMove;
import com.puzzletimer.graphics.Panel3D;
import com.puzzletimer.graphics.Vector3;

/**
 * @author Michael Mikolajczyk
 * @version $Revision: $, $Date: $, $Author: $
 */
public class CubeView extends Panel3D implements Observer {
  private static final long serialVersionUID = 1L;

  /**
   * Create a 3D view of given RubiksCube with apllied algo for algo
   */
  public CubeView() {
    super();
    setLayout(new BorderLayout());
    setCameraPosition(new Vector3(0, 0, -1.8));
    setBorder(Utilities.createTitledBorder("The Rubik's Cube"));
    setFocusable(false);
  }

  @Override
  public void update(Observable observable, Object arg) {
    if (observable instanceof CubeModel) {
      CubeModel cubeModel = (CubeModel) observable;
      System.out.println("Update CubeView - " + observable);

      Algo tmpAlgo = calculateTemporaryAlgoMoves(cubeModel.getAlgo(), cubeModel.getLastLayerMove());
      ColorScheme tmpColorScheme = calculateTemporaryColorScheme(cubeModel.getColorScheme(), cubeModel.getCubeRotation());

      setMesh(RubiksCube.getScrambledPuzzleMesh(tmpColorScheme, tmpAlgo));
    }
  }

  private Algo calculateTemporaryAlgoMoves(Algo algo, LastLayerMove lastLayerMove) {
    Algo tmpAlgo = algo.clone(); // Create a temporary list with algo moves to avoid changes in original algo
    switch (lastLayerMove) {
      case NO:
        return tmpAlgo;
      case RANDOM:
        LastLayerMove randomLastLayerMove = LastLayerMove.randomLastLayerMove();
        tmpAlgo = calculateTemporaryAlgoMoves(algo, randomLastLayerMove);
        return tmpAlgo;
      default:
        String appendMove = lastLayerMove.getAppendMove();
        tmpAlgo.add(appendMove);
        return tmpAlgo;
    }
  }

  private ColorScheme calculateTemporaryColorScheme(ColorScheme colorScheme, CubeRotation cubeRotation) {
    // Create just temporary copies for colorSchemes to NOT changes the original colorScheme
    ColorScheme tmpColorScheme = null;
    switch (cubeRotation) {
      case NO:
        tmpColorScheme = colorScheme.clone();
        return tmpColorScheme;
      case Y:
        tmpColorScheme = ColorScheme.createDefinedColorScheme(colorScheme.getUpColor(), colorScheme.getDownColor(), colorScheme.getRightColor(), colorScheme.getBackColor(), colorScheme.getLeftColor(), colorScheme.getFrontColor());
        return tmpColorScheme;
      case Y_2:
        tmpColorScheme = ColorScheme.createDefinedColorScheme(colorScheme.getUpColor(), colorScheme.getDownColor(), colorScheme.getBackColor(), colorScheme.getLeftColor(), colorScheme.getFrontColor(), colorScheme.getRightColor());
        return tmpColorScheme;
      case Y_PRIME:
        tmpColorScheme = ColorScheme.createDefinedColorScheme(colorScheme.getUpColor(), colorScheme.getDownColor(), colorScheme.getLeftColor(), colorScheme.getFrontColor(), colorScheme.getRightColor(), colorScheme.getBackColor());
        return tmpColorScheme;
      case RANDOM:
        CubeRotation randomCubeRotation = CubeRotation.randomCubeRotation();
        tmpColorScheme = calculateTemporaryColorScheme(colorScheme, randomCubeRotation);
        return tmpColorScheme;
    }
    return null; // Should not happen
  }
}
