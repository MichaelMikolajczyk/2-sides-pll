
package com.mmi.pllTrainer.gui.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import com.mmi.pllTrainer.gui.frame.ColorSchemeConfigurationFrame;
import com.mmi.pllTrainer.gui.listener.WindowCloseListener;
import com.mmi.pllTrainer.gui.model.Category;
import com.mmi.pllTrainer.gui.model.CubeModel;

/**
 * @author Michael Mikolajczyk
 * @version $Revision: $, $Date: $, $Author: $
 */
public class MenuBar extends JMenuBar {
  private static final long serialVersionUID = 1L;

  private CubeModel cubeModel;
  private JFrame mainFrame;

  public MenuBar(JFrame mainFrame, CubeModel cubeModel) {
    this.mainFrame = mainFrame;
    this.cubeModel = cubeModel;

    JMenu fileMenu = createFileMenu();
    add(fileMenu);

    JMenu settingMenu = createOptionsMenu();
    add(settingMenu);

    JMenu categoryMenu = createTrainingMenu();
    add(categoryMenu);
  }

  private JMenu createFileMenu() {
    JMenu fileMenu = new JMenu("File");

    JMenuItem exitItem = new JMenuItem("Exit");
    exitItem.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        new WindowCloseListener(MenuBar.this.cubeModel).windowClosing(null);
      }
    });

    fileMenu.add(exitItem);
    return fileMenu;
  }

  private JMenu createOptionsMenu() {
    JMenu settingMenu = new JMenu("Options");

    JMenuItem colorSchemeItem = new JMenuItem("Color scheme...");
    colorSchemeItem.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        ColorSchemeConfigurationFrame colorSchemeConfigurationFrame = new ColorSchemeConfigurationFrame(MenuBar.this.mainFrame, MenuBar.this.cubeModel);
        colorSchemeConfigurationFrame.showView();
      }
    });

    settingMenu.add(colorSchemeItem);
    return settingMenu;
  }

  private JMenu createTrainingMenu() {
    JMenu trainingMenu = new JMenu("Training");

    Category[] categories = Category.values();
    for (final Category category : categories) {
      CategoryMenuItem menuItem = new CategoryMenuItem(this.cubeModel, category);
      menuItem.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          MenuBar.this.cubeModel.changeCategory(category);
        }
      });
      trainingMenu.add(menuItem);
    }

    return trainingMenu;
  }
}
