
package com.mmi.pllTrainer.gui.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import com.mmi.pllTrainer.gui.model.CubeModel;
import com.mmi.pllTrainer.gui.util.Utilities;
import com.mmi.pllTrainer.puzzle.common.PLL;

/**
 * @author Michael Mikolajczyk
 * @version $Revision: $, $Date: $, $Author: $
 */
public class PLLView extends JPanel implements Observer {
  private static final long serialVersionUID = 1L;

  private CubeModel cubeModel;
  private JScrollPane scrollPane;

  public PLLView(CubeModel cubeModel) {
    this.cubeModel = cubeModel;
    this.cubeModel.addObserver(this);
    setBorder(Utilities.createTitledBorder("Permutations of the last layer"));
    setLayout(new BorderLayout());

    createPLLViewParts();
  }

  private void createPLLViewParts() {
    List<PLLView.PLLCategory> pllCategories = groupByPLLCategory(this.cubeModel.getDisplayAblePLLs());

    int rows = pllCategories.size();
    JPanel pnlPLLs = new JPanel(new GridLayout(rows, 1, 10, 5));
    for (PLLCategory pllCategory : pllCategories) {
      PLLViewPart pllViewPart = new PLLViewPart(pllCategory);
      pnlPLLs.add(pllViewPart);
    }
    this.scrollPane = new JScrollPane(pnlPLLs);
    this.scrollPane.setFocusable(false);
    this.scrollPane.setPreferredSize(new Dimension(0, 0)); // Do not let window grow to max height
    this.scrollPane.getVerticalScrollBar().setUnitIncrement(20);
    this.scrollPane.setWheelScrollingEnabled(true);
    add(this.scrollPane, BorderLayout.CENTER);
    validate();
  }

  @Override
  public void update(Observable o, Object arg) {
    System.out.println("Update PLL View");
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        int scrollBarValue = PLLView.this.scrollPane.getVerticalScrollBar().getValue();
        removeAll();
        createPLLViewParts();
        PLLView.this.scrollPane.getVerticalScrollBar().setValue(scrollBarValue);
      }
    });
  }

  private List<PLLCategory> groupByPLLCategory(List<PLL> plls) {
    List<PLLView.PLLCategory> pllCategories = new ArrayList<PLLView.PLLCategory>();
    for (PLL pll : plls) {
      String name = pll.name().substring(0, 1);
      PLLCategory pllCat = new PLLCategory(name);

      if (pllCategories.contains(pllCat)) {
        for (PLLCategory cat : pllCategories) {
          if (cat.equals(pllCat)) {
            cat.addPLL(pll);
          }
        }
      } else {
        pllCat.addPLL(pll);
        pllCategories.add(pllCat);
      }

    }
    return pllCategories;
  }

  private class PLLViewPart extends JPanel {
    private static final long serialVersionUID = 1L;
    private PLLCategory pllCategory;

    public PLLViewPart(PLLCategory pllCategory) {
      this.pllCategory = pllCategory;
      setLayout(new BorderLayout(10, 10));

      JLabel lblPLLName = new JLabel(" " + this.pllCategory.getName() + " Perm" + (this.pllCategory.getPlls().size() > 1 ? "s" : ""));
      lblPLLName.setFont(Utilities.createFont(Font.BOLD, 16));
      add(lblPLLName, BorderLayout.PAGE_START);

      JPanel pnlIcon = new JPanel(new FlowLayout(FlowLayout.LEFT, 20, 5));
      String resourceBasePath = "/com/mmi/pllTrainer/gui/resources/pll/";
      for (final PLL pll : this.pllCategory.getPlls()) {
        URL resource = getClass().getResource(resourceBasePath + pll.name() + ".gif");
        ImageIcon icon = new ImageIcon(resource);
        JButton btnIcon = new JButton(icon);
        btnIcon.setFocusable(false);
        btnIcon.setPreferredSize(new Dimension(icon.getIconWidth() + 10, icon.getIconHeight() + 10));
        btnIcon.setToolTipText(pll.getSolutionAlgo());
        btnIcon.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            if (PLLView.this.cubeModel.getPLL() == pll) {
              PLLView.this.cubeModel.nextPLL();
            }
          }
        });
        pnlIcon.add(btnIcon);
      }

      add(pnlIcon);
    }
  }

  private class PLLCategory {
    private String name;
    private List<PLL> plls = new ArrayList<PLL>();

    public PLLCategory(String name) {
      this.name = name;
    }

    public void addPLL(PLL pll) {
      plls.add(pll);
    }

    public String getName() {
      return this.name;
    }

    public List<PLL> getPlls() {
      return plls;
    }

    @Override
    public String toString() {
      StringBuilder builder = new StringBuilder(name);
      builder.append(this.plls.toString());
      return builder.toString();
    }

    @Override
    public boolean equals(Object o) {
      if (!(o instanceof PLLCategory)) {
        return false;
      }
      PLLCategory other = (PLLCategory) o;
      return other.getName().equals(this.getName());
    }

    @Override
    public int hashCode() {
      return this.name.hashCode();
    }
  }

}
