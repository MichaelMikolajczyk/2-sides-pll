/*
 * Copyright (c) 2014 SSI Schaefer Noell GmbH
 *
 * $Id: $ / $HeadURL: $
 */

package com.mmi.pllTrainer.gui.view;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JCheckBoxMenuItem;

import com.mmi.pllTrainer.gui.model.Category;
import com.mmi.pllTrainer.gui.model.CubeModel;

/**
 * @author <a href="mailto:mikolajczyk@ssi-schaefer-noell.com">mikolajczyk</a>
 * @version $Revision: $, $Date: $, $Author: $
 */
public class CategoryMenuItem extends JCheckBoxMenuItem implements Observer {
  private static final long serialVersionUID = 1L;

  private final Category category;
  private final CubeModel cubeModel;

  public CategoryMenuItem(CubeModel cubeModel, Category category) {
    super(category.getText());
    this.cubeModel = cubeModel;
    this.category = category;
    this.cubeModel.addObserver(this);
  }

  @Override
  public void update(Observable o, Object arg) {
    setSelected(this.cubeModel.getCategory() == this.category);
  }

}
