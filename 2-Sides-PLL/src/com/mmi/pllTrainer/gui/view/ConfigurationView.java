
package com.mmi.pllTrainer.gui.view;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.mmi.pllTrainer.gui.listener.CubeRotationConfigListener;
import com.mmi.pllTrainer.gui.listener.LastLayerMoveConfigListener;
import com.mmi.pllTrainer.gui.model.CubeModel;
import com.mmi.pllTrainer.gui.util.Utilities;
import com.mmi.pllTrainer.puzzle.common.CubeRotation;
import com.mmi.pllTrainer.puzzle.common.LastLayerMove;

/**
 * @author Michael Mikolajczyk
 * @version $Revision: $, $Date: $, $Author: $
 */
public class ConfigurationView extends JPanel {
  private static final long serialVersionUID = 1L;

  private CubeModel cubeModel;

  private Dimension configLabelDim;
  private Dimension checkBoxDim;

  public ConfigurationView(CubeModel cubeModel) {
    this.configLabelDim = new Dimension(85, 25);
    this.checkBoxDim = new Dimension(65, 25);

    this.cubeModel = cubeModel;

    setLayout(new GridLayout(2, 2));
    setBorder(Utilities.createTitledBorder("Configuration"));

    createCubeRotationPropertyViewPart();

    createLastLayerMovePropertyViewPart();
  }

  private void createLastLayerMovePropertyViewPart() {
    JPanel lastLayerMovePanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 20, 10));
    lastLayerMovePanel.setLayout(new GridLayout(1, 5, 5, 5));

    JLabel label = new JLabel("Additional Turn:");
    label.setFont(Utilities.createFontLabel());
    label.setPreferredSize(this.configLabelDim);
    lastLayerMovePanel.add(label);

    LastLayerMoveConfigListener listener = new LastLayerMoveConfigListener(this.cubeModel);

    ButtonGroup group = new ButtonGroup();
    for (LastLayerMove lastLayerMove : LastLayerMove.values()) {
      JCheckBox checkBox = new JCheckBox(lastLayerMove.getSelectionText());
      checkBox.setFont(Utilities.createFontLabel());
      checkBox.setPreferredSize(checkBoxDim);
      checkBox.setBackground(lastLayerMovePanel.getBackground());
      group.add(checkBox);
      lastLayerMovePanel.add(checkBox);
      checkBox.setFocusable(false);
      checkBox.addItemListener(listener);
      checkBox.setSelected(this.cubeModel.getLastLayerMove().name().equals(lastLayerMove.name()));
    }

    add(lastLayerMovePanel);
  }

  private void createCubeRotationPropertyViewPart() {
    JPanel cubeRotationPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 20, 10));
    cubeRotationPanel.setLayout(new GridLayout(1, 5, 5, 5));

    JLabel label = new JLabel("Cube Rotation:");
    label.setFont(Utilities.createFontLabel());
    label.setPreferredSize(this.configLabelDim);
    cubeRotationPanel.add(label);

    CubeRotationConfigListener listener = new CubeRotationConfigListener(this.cubeModel);

    ButtonGroup group = new ButtonGroup();
    for (CubeRotation cubeRotation : CubeRotation.values()) {
      JCheckBox checkBox = new JCheckBox(cubeRotation.getSelectionText());
      checkBox.setFont(Utilities.createFontLabel());
      checkBox.setPreferredSize(checkBoxDim);
      checkBox.setBackground(cubeRotationPanel.getBackground());
      group.add(checkBox);
      cubeRotationPanel.add(checkBox);
      checkBox.setFocusable(false);
      checkBox.addItemListener(listener);
      checkBox.setSelected(this.cubeModel.getCubeRotation().name().equals(cubeRotation.name()));
    }

    add(cubeRotationPanel);
  }
}
