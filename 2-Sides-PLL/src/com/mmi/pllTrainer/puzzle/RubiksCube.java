
package com.mmi.pllTrainer.puzzle;

import java.awt.Color;
import java.util.HashMap;

import com.mmi.pllTrainer.puzzle.common.Algo;
import com.mmi.pllTrainer.puzzle.common.ColorScheme;
import com.mmi.pllTrainer.puzzle.common.Twist;
import com.puzzletimer.graphics.Matrix44;
import com.puzzletimer.graphics.Mesh;
import com.puzzletimer.graphics.Plane;
import com.puzzletimer.graphics.Vector3;

public class RubiksCube {

  public static Mesh getScrambledPuzzleMesh(ColorScheme colorScheme, Algo algo) {
    Plane planeL = new Plane(new Vector3(-0.166, 0, 0), new Vector3(-1, 0, 0));
    Plane planeR = new Plane(new Vector3(0.166, 0, 0), new Vector3(1, 0, 0));
    Plane planeD = new Plane(new Vector3(0, -0.166, 0), new Vector3(0, -1, 0));
    Plane planeU = new Plane(new Vector3(0, 0.166, 0), new Vector3(0, 1, 0));
    Plane planeF = new Plane(new Vector3(0, 0, -0.166), new Vector3(0, 0, -1));
    Plane planeB = new Plane(new Vector3(0, 0, 0.166), new Vector3(0, 0, 1));

    Mesh mesh = Mesh.cube(new Color[] { //
      colorScheme.getLeftColor(), // LEFT
        colorScheme.getBackColor(), // BACK
        colorScheme.getDownColor(), // DOWN
        colorScheme.getRightColor(), // RIGHT
        colorScheme.getFrontColor(), // FRONT
        colorScheme.getUpColor(), // UP
      });

    mesh = mesh //
      .cut(planeL, 0) //
      .cut(planeR, 0) //
      .cut(planeD, 0) //
      .cut(planeU, 0) //
      .cut(planeF, 0) //
      .cut(planeB, 0) //
      .shortenFaces(0.03) //
      .softenFaces(0.015) //
      .softenFaces(0.005);

    HashMap<String, Twist> twists = new HashMap<String, Twist>();
    twists.put("L", new Twist(planeL, Math.PI / 2));
    twists.put("L2", new Twist(planeL, Math.PI));
    twists.put("L'", new Twist(planeL, -Math.PI / 2));
    twists.put("R", new Twist(planeR, Math.PI / 2));
    twists.put("R2", new Twist(planeR, Math.PI));
    twists.put("R'", new Twist(planeR, -Math.PI / 2));
    twists.put("D", new Twist(planeD, Math.PI / 2));
    twists.put("D2", new Twist(planeD, Math.PI));
    twists.put("D'", new Twist(planeD, -Math.PI / 2));
    twists.put("U", new Twist(planeU, Math.PI / 2));
    twists.put("U2", new Twist(planeU, Math.PI));
    twists.put("U'", new Twist(planeU, -Math.PI / 2));
    twists.put("F", new Twist(planeF, Math.PI / 2));
    twists.put("F2", new Twist(planeF, Math.PI));
    twists.put("F'", new Twist(planeF, -Math.PI / 2));
    twists.put("B", new Twist(planeB, Math.PI / 2));
    twists.put("B2", new Twist(planeB, Math.PI));
    twists.put("B'", new Twist(planeB, -Math.PI / 2));

    // Apply PLL
    for (String move : algo.getAlgoMoves()) {
      Twist t = twists.get(move);
      if (t != null) {
        mesh = mesh.rotateHalfspace(t.getPlane(), t.getAngle());
      } else {
        System.out.println("WARNING: Cannot apply algo move <" + move + "> -> Skip it...");
      }
    }

    // custom perspective view
    mesh = mesh.transform(Matrix44.rotationY(-Math.PI / 5)).transform(Matrix44.rotationX(Math.PI / 7));

    return mesh;
  }

}
