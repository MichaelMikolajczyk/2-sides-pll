
package com.mmi.pllTrainer.puzzle.common;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mmi.pllTrainer.gui.model.CubeModel;

/**
 * @author Michael Mikolajczyk
 * @version $Revision: $, $Date: $, $Author: $
 */
public class ColorScheme implements Cloneable {
  private static final String FACE_FRONT = "Face-F";
  private static final String FACE_BACK = "Face-B";
  private static final String FACE_LEFT = "Face-L";
  private static final String FACE_RIGHT = "Face-R";
  private static final String FACE_UP = "Face-U";
  private static final String FACE_DOWN = "Face-D";

  // Face color map with color and color names
  private Map<String, Color> map = new HashMap<String, Color>();

  public ColorScheme() {
    this.map.put(ColorScheme.FACE_LEFT, new Color(255, 127, 0));
    this.map.put(ColorScheme.FACE_RIGHT, new Color(255, 0, 0));
    this.map.put(ColorScheme.FACE_FRONT, new Color(0, 0, 139));
    this.map.put(ColorScheme.FACE_BACK, new Color(0, 238, 0));
    this.map.put(ColorScheme.FACE_UP, new Color(255, 255, 0));
    this.map.put(ColorScheme.FACE_DOWN, new Color(255, 255, 255));
  }

  private ColorScheme(Color colorUp, Color colorDown, Color colorFront, Color colorRight, Color colorBack, Color colorLeft) {
    this.map.put(ColorScheme.FACE_LEFT, colorLeft);
    this.map.put(ColorScheme.FACE_RIGHT, colorRight);
    this.map.put(ColorScheme.FACE_FRONT, colorFront);
    this.map.put(ColorScheme.FACE_BACK, colorBack);
    this.map.put(ColorScheme.FACE_UP, colorUp);
    this.map.put(ColorScheme.FACE_DOWN, colorDown);
  }

  public Color getLeftColor() {
    return this.map.get(ColorScheme.FACE_LEFT);
  }

  public Color getRightColor() {
    return this.map.get(ColorScheme.FACE_RIGHT);
  }

  public Color getFrontColor() {
    return this.map.get(ColorScheme.FACE_FRONT);
  }

  public Color getBackColor() {
    return this.map.get(ColorScheme.FACE_BACK);
  }

  public Color getUpColor() {
    return this.map.get(ColorScheme.FACE_UP);
  }

  public Color getDownColor() {
    return this.map.get(ColorScheme.FACE_DOWN);
  }

  public Color getColorForFace(String face) {
    return this.map.get(face);
  }

  @Override
  public ColorScheme clone() {
    return new ColorScheme(getUpColor(), getDownColor(), getFrontColor(), getRightColor(), getBackColor(), getLeftColor());
  }

  public Color[] getSortedColors() {
    List<Color> colors = new ArrayList<Color>();
    colors.add(getLeftColor()); // LEFT
    colors.add(getBackColor()); // BACK
    colors.add(getDownColor()); // DOWN
    colors.add(getRightColor()); // RIGHT
    colors.add(getFrontColor()); // FRONT
    colors.add(getUpColor()); // UP
    return (Color[]) colors.toArray();
  }

  public static List<String> getSortedFaces() {
    List<String> faces = new ArrayList<String>();
    faces.add(ColorScheme.FACE_UP);
    faces.add(ColorScheme.FACE_DOWN);
    faces.add(ColorScheme.FACE_FRONT);
    faces.add(ColorScheme.FACE_RIGHT);
    faces.add(ColorScheme.FACE_BACK);
    faces.add(ColorScheme.FACE_LEFT);
    return faces;
  }

  /**
   * Use {@link CubeModel#updateColorForFace(String, Color)} if you want update the cubeView on main frame.
   * @param face - the face to be updated
   * @param newColor - the new color to set for given face 
   */
  public void updateColorForFace(String face, Color newColor) {
    this.map.put(face, newColor);
  }

  public static ColorScheme createDefinedColorScheme(Color colorUp, Color colorDown, Color colorFront, Color colorRight, Color colorBack, Color colorLeft) {
    ColorScheme colorScheme = new ColorScheme(colorUp, colorDown, colorFront, colorRight, colorBack, colorLeft);
    return colorScheme;
  }

}
