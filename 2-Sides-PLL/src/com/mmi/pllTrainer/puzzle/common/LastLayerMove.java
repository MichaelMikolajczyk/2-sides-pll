
package com.mmi.pllTrainer.puzzle.common;

import java.util.Random;

/**
 * @author Michael Mikolajczyk
 * @version $Revision: $, $Date: $, $Author: $
 */

public enum LastLayerMove {
  NO("No turn", ""), //
    U("U", "U"), //
    U_2("U2", "U2"), //
    U_PRIME("U'", "U'"), //
    RANDOM("Random", "");

  private String selectionText;
  private String appendMove;
  private static Random random = new Random();

  private LastLayerMove(String selectionText, String appendMove) {
    this.selectionText = selectionText;
    this.appendMove = appendMove;
  }

  public String getSelectionText() {
    return selectionText;
  }

  public String getAppendMove() {
    return appendMove;
  }

  public static LastLayerMove randomLastLayerMove() {
    LastLayerMove[] values = new LastLayerMove[] { LastLayerMove.NO, LastLayerMove.U, LastLayerMove.U_2, LastLayerMove.U_PRIME };
    int randomIndex = random.nextInt(values.length);
    return values[randomIndex];
  }

}
