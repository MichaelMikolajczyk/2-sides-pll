
package com.mmi.pllTrainer.puzzle.common;

import java.util.Random;

/**
 * @author Michael Mikolajczyk
 * @version $Revision: $, $Date: $, $Author: $
 */
public enum CubeRotation {
  NO("No rotation"), //
    Y("y"), //
    Y_2("y2"), //
    Y_PRIME("y'"), // 
    RANDOM("Random");

  private String selectionText;
  private static Random random = new Random();

  private CubeRotation(String selectionText) {
    this.selectionText = selectionText;
  }

  public String getSelectionText() {
    return selectionText;
  }

  public static CubeRotation randomCubeRotation() {
    CubeRotation[] values = new CubeRotation[] { CubeRotation.NO, CubeRotation.Y, CubeRotation.Y_2, CubeRotation.Y_PRIME };
    int randomIndex = random.nextInt(values.length);
    return values[randomIndex];
  }

}
