
package com.mmi.pllTrainer.puzzle.common;

import com.puzzletimer.graphics.Plane;

public class Twist {

  private Plane plane;
  private double angle;

  public Twist(Plane plane, double angle) {
    this.plane = plane;
    this.angle = angle;
  }

  public Plane getPlane() {
    return plane;
  }

  public double getAngle() {
    return angle;
  }
}
