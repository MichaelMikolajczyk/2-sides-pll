
package com.mmi.pllTrainer.puzzle.common;

/**
 * @author Michael Mikolajczyk
 * @version $Revision: $, $Date: $, $Author: $
 */
public enum PLL {
  //
    A1("R2 B2 R F R' B2 R F' R", //
      "x R' F R' B2 R F' R' B2 R2"), //
    A2("R2 F2 R' B' R F2 R' B R'", //
      "x R B' R F2 R' B R F2 R2"), // 

    E("R B' R' F R B R' F' R B R' F R B' R' F'", //
      "x' (R U' R') D (R U R') D' (R U R') D (R U' R') D'"), // 

    F("R' U' F' R U R' U' R' F R2 U' R' U' R U R' U R", //
      "R' U' F' (R U R' U') (R' F) R2 U' R' U' R U R' U R"), //

    G1("U R U R' F2 D' L U' L' U L' D F2 U'", //
      "R2 u' R U' R U R' u R2 B U' B'"), //
    G2("R' U' R U D' R2 U R' U R U' R U' R2 U' D R' U' R U D' R2 U R' U R U' R U' R2 U' D", //
      "(R' U' R) (U D') R2 U R' U R U' R U' R2 (U' D)"), //
    G3("F2 D' L U' L U L' D F2 R U' R'", //
      "(R U R') (U' D) R2 U' R U' R' U R' U R2 (U D')"), //
    G4("U' R' U' R B2 D L' U L U' L D' B2 U", //
      "R2 u R' U R' U' R u' R2 F' U F"), //

    H("R2 U2 R U2 R2 U2 R2 U2 R U2 R2", //
      "R2 U2 R U2 R2 U2 R2 U2 R U2 R2"), //

    J1("R U R' F' R U R' U' R' F R2 U' R' U'", //
      "(R U R') F' (R U R' U') (R' F) R2 U' R' U'"), //
    J2("L U' R' U L' U2 R U' R' U2 R", //
      "R' U2 R U R' U z R U R' D R U'"), //

    N1("R U R' U R U R' F' R U R' U' R' F R2 U' R' U2 R U' R'", //
      "(R U R' U) (R U R' F') (R U R' U') (R' F) R2 U' R' U2 R U' R'"), // 
    N2("R' U R U' R' F' U' F R U R' F R' F' R U' R", //
      "(R' U R U') R' F' U' F (R U R') F R' F' R U' R"), //

    R1("R' U2 R U2 R' F R U R' U' R' F' R2 U'", //
      "R' U2 R U2 R' F R U R' U' R' F' R2 U'"), //
    R2("R U R' F' R U2 R' U2 R' F R U R U2 R' U'", //
      "R U R' F' R U2 R' U2 R' F R U R U2 R' U'"), //

    T("R U R' U' R' F R2 U' R' U' R U R' F'", //
      "(R U R' U') (R' F) R2 U' R' U' (R U R') F'"), //

    U1("R2 U' R' U' R U R U R U' R", //
      "R' U R' U' R' U' R' U R U R2"), //
    U2("R' U R' U' R' U' R' U R U R2", //
      "R2 U' R' U' R U R U R U' R"), //

    V("R' U R' U' B' R' B2 U' B' U B' R B R", //
      "(R' U R') d' R' F' R2 U' R' U R' F R F"), //

    Y("F R U' R' U' R U R' F' R U R' U' R' F R F'", //
      "F R' F R2 U' R' U' (R U R') F' (R U R' U') F'"), //

    Z("U R' U' R U' R U R U' R' U R U R2 U' R' U", //
      "U R' U' (R U' R) U (R U' R' U) R U R2 U' R' U");

  private Algo algo;
  private final String solutionAlgo;

  private PLL(String algorhythm, String solutionAlgo) {
    this.solutionAlgo = solutionAlgo;
    this.algo = new Algo(algorhythm);
  }

  public Algo getAlgo() {
    return algo;
  }

  public String getSolutionAlgo() {
    return this.solutionAlgo;
  }

}
