
package com.mmi.pllTrainer.puzzle.common;

import java.util.ArrayList;
import java.util.List;

public class Algo implements Cloneable {

  private List<String> algoMoves;

  public Algo() {
    this("");
  }

  public Algo(String algo) {
    algoMoves = new ArrayList<String>();
    parseSequence(algo);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder("Algo: ");
    for (String move : this.algoMoves) {
      builder.append(move + " ");
    }
    builder.append("\n");
    return builder.toString();
  }

  private void parseSequence(String algo) {
    if (!algo.trim().isEmpty()) {
      String[] splitAlgo = algo.split(" ");
      for (String move : splitAlgo) {
        this.add(move);
      }
    }
  }

  public void add(String move) {
    this.algoMoves.add(move);
  }

  public List<String> getAlgoMoves() {
    return this.algoMoves;
  }

  public String getCompleteSequence() {
    StringBuilder builder = new StringBuilder();
    for (String move : this.algoMoves) {
      builder.append(move).append(" ");
    }
    return builder.toString().trim();
  }

  //  public static Algo copy(Algo algo) {
  //    return new Algo(algo.getCompleteSequence());
  //  }

  @Override
  public Algo clone() {
    return new Algo(getCompleteSequence());
  }
}
